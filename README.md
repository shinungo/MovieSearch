**** MovieSearch **** *** Semesterprojekt Android *** This is a Android App to get Information about a certain Movie which is in a Database and reachable via API

/**

Written by
Markus Bolliger & Farhad Merzaie
©-ZHAW-27.09.2019 **/
GIT: https://github.com/shinungo/MovieDirectory.git

CLASS MainActivity: Instantiate a "RecyclerView" (for the List), a "MovieRecyclerView" For the "View", a List of Movies, a Queue for Requests and some Dialogs

In Method: onCreate

creates the Layout, Toolbar. Instantiates
a queue for the search Terms.
Views
Search String
Prefs
Makes the fab "showingInputDialog" by clicking it.
Method: showInputDialog

creates a GUI for the user to make a search input.
Method getMovies: -combines search-Term with API-URL-Target
-transmits it - Stores - as long as it has - the response of URL in a JSON Array List - - handle's this to the movie List.

The movieRecyclerViewAdapter notifies Changes via method notifyDataSetChanged(). And returns the movie List.

CLASS MovieDetailActivity: By Clicking on a "Movie", this Class get's the Movie-Detail-Data. In Method "getMovieDetails" it hand's over the IMDB-Id, for getting More Information of that Movie from a defined API-Adresse, similar to Main Class.

CLASS MovieRecyclerViewAdapter: This Class makes the "Main"-View for displayed Movies. It is called recycled view Adapter, because we use it for each Movie which will be displayed in the "Main"-View. We use the Picasso Library for loading Pictures (or Placeholder) to the "poster"image-View.

It has a n Inner class "ViewHolder", which puts all the Content (e.g. Position) - as well as the Context of the "search" to each single Movie-Object. It makes this Object clickable to get desired Details of that Movie.

CLASS:Movie: Is the Model of a Movie. It provides the Information via Getter and Setter Methods.

Class Constants: Provides the API-Adresses as URL's to make the search request Possible. It also contains a API-Key to have Access to the external Database.

CLASS Prefs: Provides a Search String, which has a Placeholder in example "Batman" - to have something to show at first use.

Layout's & Others: -activity_main is the base Layout.
-Movie_Row, activity_main and activity_movie_detail use CardViews to display details of the movies. -dialog_view provides the Elements for the "search-GUI".

Manifest: Uses Permissions for Internet.
