package ch.shinungo.android.zhaw_movie_db_catcher.Util;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 *  Written by
 *  Markus Bolliger & Farhad Merzaie
 *  ©-ZHAW-27.09.2019
 **/


public class Prefs {
    SharedPreferences sharedPreferences;

    public Prefs(Activity activity) {
        sharedPreferences = activity.getPreferences(activity.MODE_PRIVATE);
    }

    public void setSearch(String search) {
        sharedPreferences.edit().putString("search", search).commit();
    }

    public String getSearch() {
        return sharedPreferences.getString("search", "Matrix");

    }
}