package ch.shinungo.android.zhaw_movie_db_catcher.Util;

/**
 *  Written by
 *  Markus Bolliger & Farhad Merzaie
 *  ©-ZHAW-27.09.2019
 **/

public class Constants {

    public static final String URL_LEFT = "http://www.omdbapi.com/?s=";
    public static final String URL = "http://www.omdbapi.com/?i=";
    public static final String API_KEY = "&apikey=62d30e92";
    public static final String URL_RIGHT = "&page=1";

}